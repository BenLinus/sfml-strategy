#include <ctime>

#include "src/draftsman.h"

int main() {
    sf::RenderWindow window(sf::VideoMode(600, 600), "Free Strategy!");
    window.setFramerateLimit(5);
    
    init();
    Map *map = new Map();
    
//    srand(time(NULL));
//    short int type;
//    short int spriteNum;
//    
//    for (unsigned char i = 0; i < 10; i++)
//    {
//        unsigned char x = rand() % 16;
//        unsigned char y = rand() % 16;
//        
//        type = rand() % 2;
//        
//        if (type == 1)
//        {
//            spriteNum = 2;
//        }
//        else
//        {
//            spriteNum = 3;
//        }
//        
//        Character *charTest = new Character(spriteNum);
//        charTest->x = x;
//        charTest->y = y;
//
//        map->addCharacter(*charTest);
//    }
//    
//    map->save(1);
    map->load(1);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            
            if (event.type == sf::Event::KeyReleased)
            {
                Character *newChar = new Character(3);
                newChar->x = 5;
                newChar->y = 5;

                map->addCharacter(*newChar);
            }
        }

        window.clear();
        drawMap(*map, window);
        window.display();
        
        // need to fix CPU 100%
        sf::sleep(sf::milliseconds(1500));
    }

    return 0;
}