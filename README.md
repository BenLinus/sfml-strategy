# Compile a source
```
g++ -m64    -o dist/Debug/GNU-Linux/game build/Debug/GNU-Linux/main.o build/Debug/GNU-Linux/src/config.o build/Debug/GNU-Linux/src/data.o build/Debug/GNU-Linux/src/draftsman.o build/Debug/GNU-Linux/src/helper.o build/Debug/GNU-Linux/src/logic.o build/Debug/GNU-Linux/src/map.o -Llib -Wl,-rpath,'lib' -lsfml-graphics -lsfml-audio -lsfml-network -lsfml-window -lsfml-system
```