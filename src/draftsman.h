/* 
 * File:   draftsman.h
 * Author: dementor
 *
 * Created on 17 октября 2019 г., 0:00
 */

#ifndef DRAFTSMAN_H
#define DRAFTSMAN_H

#include <SFML/Graphics.hpp>
#include <string>

#include "map.h"

void drawMap (Map &m, sf::RenderWindow &window);

#endif /* DRAFTSMAN_H */

