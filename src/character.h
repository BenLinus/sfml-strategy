/* 
 * File:   character.h
 * Author: dementor
 *
 * Created on 17 ноября 2019 г., 22:24
 */

#ifndef CHARACTER_H
#define CHARACTER_H

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "data.h"

class Character
{
public:
    Character();
    Character(short int spriteNum);
    Character(const Character &ch);
    ~Character();
    sf::Sprite sprite;
    sf::Texture texture;
    short int spriteNum;
    int health;
    bool canMove;
    int x;
    int y;
};

#endif /* CHARACTER_H */

