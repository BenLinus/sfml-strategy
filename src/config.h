/* 
 * File:   config.h
 * Author: dementor
 *
 * Created on 17 октября 2019 г., 0:13
 */

#ifndef CONFIG_H
#define CONFIG_H

const unsigned char MAX_MAPS = 8;
const unsigned char MAX_PLAYERS = 10;
const unsigned char MAX_MAP_X = 32;
const unsigned char MAX_MAP_Y = 32;

#endif /* CONFIG_H */

