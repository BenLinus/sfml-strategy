/* 
 * File:   data.h
 * Author: dementor
 *
 * Created on 17 октября 2019 г., 0:39
 */

#ifndef DATA_H
#define DATA_H

#include "config.h"
#include "helper.h"
#include "map.h"

class Map;

enum SPRITE_TYPES
{
    TYPE_GRASS = 0,
    TYPE_WATER = 1,
    TYPE_BUILDING = 2,
    TYPE_PLAYER = 3
};

extern Map maps[MAX_MAPS];
extern sf::Sprite sprites[4];
extern sf::Texture textures[4];

void init();

#endif /* DATA_H */

