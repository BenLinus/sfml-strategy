#include "draftsman.h"

void drawMap (Map &m, sf::RenderWindow &window)
{
    for (unsigned char x = 0; x < m.getMapX(); ++x)
    {
        for (unsigned char y = 0; y < m.getMapY(); ++y)
        {   
            m.getTile(x, y).sprite.setPosition(x * 32, y * 32);
            window.draw(m.getTile(x, y).sprite);
            
            for (unsigned char indexCharacter = 0; indexCharacter < m.getCountCharacters(); ++indexCharacter)
            {
                if (m.getCharacter(indexCharacter).x == x && m.getCharacter(indexCharacter).y == y)
                {
                    m.getCharacter(indexCharacter).sprite.setPosition(32 * x, 32 * y);
                    window.draw(m.getCharacter(indexCharacter).sprite);
                }
            }
        }
    }
}