/* 
 * File:   helper.h
 * Author: dementor
 *
 * Created on 17 октября 2019 г., 23:04
 */

#ifndef HELPER_H
#define HELPER_H

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <fstream>

void log (std::string data, bool output = true);

sf::Texture loadTexture (std::string fileName, sf::IntRect tile, bool smooth = true);

#endif /* HELPER_H */

