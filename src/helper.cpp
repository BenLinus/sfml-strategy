#include "helper.h"

void log(std::string data, bool output) {
    std::ofstream file("log/log.txt", std::ios_base::app);
    file << data << std::endl;
    file.close();
    
    if (output)
    {
        std::cout << data << std::endl;
    }
}

sf::Texture loadTexture(std::string fileName, sf::IntRect tile, bool smooth) {
    sf::Texture texture;

    if (!texture.loadFromFile(fileName, tile)) {
        log("Can't load texture: " + fileName);
    }
    texture.setSmooth(smooth);

    return texture;
}