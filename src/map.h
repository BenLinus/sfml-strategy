/* 
 * File:   map.h
 * Author: dementor
 *
 * Created on 17 октября 2019 г., 0:01
 */

#ifndef MAP_H
#define MAP_H

#include <SFML/Graphics.hpp>
#include <fstream>

#include "config.h"
#include "helper.h"
#include "character.h"

class Character;

enum tileType
{
    BOLD = 0
};

struct Tile
{
public:
    Tile();
    Tile (const Tile &tile);
    sf::Sprite sprite;
    short int spriteNum;
    short unsigned int type;
};

class Map {
public:
    Map();
    ~Map();
    void save(unsigned char num);
    void load(unsigned char num);
    unsigned char getMapX();
    unsigned char getMapY();
    
    void addCharacter(Character &character);
    Character& getCharacter (unsigned char num);
    
    Tile& getTile(unsigned char x, unsigned char y);
    
    unsigned char getCountCharacters();

private:
    unsigned char size_x;
    unsigned char size_y;
    short unsigned int countCharacters;
    Tile **tiles;
    std::vector<Character> characters;
};
#endif /* MAP_H */

