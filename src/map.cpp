/* 
 * File:   map.cpp
 * Author: dementor
 * 
 * Created on 17 октября 2019 г., 0:01
 */

#include "map.h"
#include "data.h"

Tile::Tile() {
    unsigned char index = rand() % 2;
    this->spriteNum = index;
    this->sprite = ::sprites[this->spriteNum];
    this->type = BOLD;
}

Tile::Tile (const Tile &tile)
{
    this->spriteNum = tile.spriteNum;
    this->type = tile.type;
    this->sprite = tile.sprite;
}

Map::Map() {
    this->size_x = MAX_MAP_X;
    this->size_y = MAX_MAP_Y;
    
    this->tiles = new Tile*[this->size_x];
    
    for (int x = 0; x < this->size_x; ++x)
    {
        this->tiles[x] = new Tile[this->size_y];
    }
    
    this->countCharacters = 0;
}

Map::~Map() {
    for (unsigned char x = 0; x < this->size_x; ++x)
    {
        delete[] this->tiles[x];
    }
    
//    for (unsigned char i = 0; i < this->countCharacters; ++i)
//    {
//        delete this->characters[i];
//    }
    
    delete[] this->tiles;
}

void Map::save(unsigned char num) {
    std::string fileName = std::to_string(num) + ".map";
    
    std::ofstream b(fileName, std::ios::out | std::ios::binary);
    b.write((char*)&this->size_x, sizeof(this->size_x));
    b.write((char*)&this->size_y, sizeof(this->size_y));
   
    b.write((char*)&this->countCharacters, sizeof(this->countCharacters));
    
    for (unsigned char x = 0; x < this->size_x; ++x) 
    {
        for (unsigned char y = 0; y < this->size_y; ++y) 
        {
            b.write((char*)&this->tiles[x][y].spriteNum, sizeof(this->tiles[x][y].spriteNum));
            b.write((char*)&this->tiles[x][y].type, sizeof(this->tiles[x][y].type));
        }
    }
    
    for (unsigned char num = 0; num < this->countCharacters; ++num)
    {
        b.write((char*)&this->characters[num].spriteNum, sizeof(this->characters[num].spriteNum));
        b.write((char*)&this->characters[num].health, sizeof(this->characters[num].health));
        b.write((char*)&this->characters[num].canMove, sizeof(this->characters[num].canMove));
        b.write((char*)&this->characters[num].x, sizeof(this->characters[num].x));
        b.write((char*)&this->characters[num].y, sizeof(this->characters[num].y));
    }
    
    b.close();
}

void Map::load(unsigned char num) {
    std::string fileName = std::to_string(num) + ".map";
    std::ifstream b(fileName, std::ios::in | std::ios::binary);
    
    b.read((char*)&this->size_x, sizeof (this->size_x));
    b.read((char*)&this->size_y, sizeof (this->size_y));
    
    b.read((char*)&this->countCharacters, sizeof (this->countCharacters));
    
    for (unsigned char x = 0; x < this->size_x; ++x)
    {
        for (unsigned char y = 0; y < this->size_y; ++y)
        {
            b.read((char*)&this->tiles[x][y].spriteNum, sizeof(this->tiles[x][y].spriteNum));
            b.read((char*)&this->tiles[x][y].type, sizeof(this->tiles[x][y].type));
            
            this->tiles[x][y].sprite = ::sprites[this->tiles[x][y].spriteNum];
        }
    }
    
    Character *tmpChars = new Character[this->countCharacters];
    
    for (unsigned char num = 0; num < this->countCharacters; ++num)
    {
        this->characters.push_back(tmpChars[num]);
        
        b.read((char*)&this->characters[num].spriteNum, sizeof(this->characters[num].spriteNum));
        b.read((char*)&this->characters[num].health, sizeof(this->characters[num].health));
        b.read((char*)&this->characters[num].canMove, sizeof(this->characters[num].canMove));
        b.read((char*)&this->characters[num].x, sizeof(this->characters[num].x));
        b.read((char*)&this->characters[num].y, sizeof(this->characters[num].y));
               
        this->characters[num].sprite = ::sprites[this->characters[num].spriteNum];
    }

    b.close();
}

void Map::addCharacter(Character &character) {
    this->characters.push_back(character);
    ++this->countCharacters;
}

unsigned char Map::getMapX() {
    return this->size_x;
}

unsigned char Map::getMapY() {
    return this->size_y;
}

Character& Map::getCharacter (unsigned char num) {
    if (this->characters.size() > num)
    {
        return this->characters[num];
    }
}

Tile& Map::getTile (unsigned char x, unsigned char y) {
    return this->tiles[x][y];
}

unsigned char Map::getCountCharacters() {
    return this->countCharacters;
}