#include "data.h"

Map maps[MAX_MAPS];
sf::Sprite sprites[4];
sf::Texture textures[4];

void init()
{
    log ("Loading textures...");
    ::textures[0] = loadTexture("assets/tiles.bmp", sf::IntRect(0, 0, 32, 32));
    ::textures[1] = loadTexture("assets/tiles.bmp", sf::IntRect(96, 32, 32, 32));
    ::textures[2] = loadTexture("assets/1.bmp", sf::IntRect(0, 0, 32, 32));
    ::textures[3] = loadTexture("assets/3.bmp", sf::IntRect(0, 0, 32, 32));
    log ("Textures loaded");
    
    log ("Loading sprites...");
    unsigned char countTextures = sizeof (textures) / sizeof(textures[0]);
    for (unsigned char i = 0; i <= countTextures; ++i)
    {
        ::sprites[i].setTexture(::textures[i]);
    }
    log ("Sprites loaded");
}