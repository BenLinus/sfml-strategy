/* 
 * File:   character.h
 * Author: dementor
 *
 * Created on 17 ноября 2019 г., 22:24
 */

#include "character.h"

Character::Character()
{
}

Character::Character(short int spriteNum)
{
    this->spriteNum = spriteNum;
    this->sprite = ::sprites[this->spriteNum];
    this->health = 100;
}

Character::Character(const Character &ch)
{
    this->sprite = ch.sprite;
    this->health = ch.health;
    this->texture = ch.texture;
    this->spriteNum = ch.spriteNum;
    this->x = ch.x;
    this->y = ch.y;
    this->canMove = ch.canMove;
}

Character::~Character()
{
//    std::cout << "char destructor" << std::endl;
}