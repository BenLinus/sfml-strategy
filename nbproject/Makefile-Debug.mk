#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/src/character.o \
	${OBJECTDIR}/src/config.o \
	${OBJECTDIR}/src/data.o \
	${OBJECTDIR}/src/draftsman.o \
	${OBJECTDIR}/src/helper.o \
	${OBJECTDIR}/src/logic.o \
	${OBJECTDIR}/src/map.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-m64
CXXFLAGS=-m64

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Llib -Wl,-rpath,'lib' -lsfml-graphics -lsfml-audio -lsfml-network -lsfml-window -lsfml-system

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/game

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/game: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	g++ -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/game ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/main.o: main.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/src/character.o: src/character.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/character.o src/character.cpp

${OBJECTDIR}/src/config.o: src/config.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/config.o src/config.cpp

${OBJECTDIR}/src/data.o: src/data.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/data.o src/data.cpp

${OBJECTDIR}/src/draftsman.o: src/draftsman.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/draftsman.o src/draftsman.cpp

${OBJECTDIR}/src/helper.o: src/helper.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/helper.o src/helper.cpp

${OBJECTDIR}/src/logic.o: src/logic.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/logic.o src/logic.cpp

${OBJECTDIR}/src/map.o: src/map.cpp nbproject/Makefile-${CND_CONF}.mk
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -s -I../../Загрузки/SFML/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/map.o src/map.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
